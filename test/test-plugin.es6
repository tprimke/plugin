var should = require('should'),
    Plugin = require('../build/plugin');

describe( 'Plugin', function() {
  
  it( 'info (empty plugin)', () => {
    let TestPlugin = Plugin( 'TestPlugin' ),
        info       = TestPlugin.info();

    let checkArray = (prop_name) => {
      info.should.have.property(prop_name);
      info[prop_name].should.be.an.Array;
      info[prop_name].length.should.eql(0);
    };

    info.should.have.property('name', 'TestPlugin');
    info.should.have.property('problems');
    checkArray( 'problems' );
    checkArray( 'solvers' );
  });

  it( 'problem', () => {
    let TestPlugin = Plugin( 'TestPlugin' );

    TestPlugin.loader( 'type1', (data) => {
      return { problem: data };
    });

    let info = TestPlugin.info();
    info.problems.length.should.eql(1);
    info.problems[0].should.eql('type1');

    let problem = TestPlugin.problem( 'type1', 'test' );
    problem.should.be.an.Object;
    problem.should.have.property('type', 'type1');
    problem.should.have.property('problem');
    problem.problem.should.be.an.Object;
    problem = problem.problem;
    problem.should.have.property('problem', 'test');
  });

  it( 'solutions (one solver)', () => {
    let TestPlugin = Plugin( 'TestPlugin' );

    TestPlugin.loader( 'type', (data) => {
      return { problem: data };
    });

    TestPlugin.solver( 'type', 'solver', (problem) => {
      return 'test';
    });

    let info = TestPlugin.info();
    info.solvers.length.should.eql(1);
    info.solvers[0].should.eql('solver');

    let problem = TestPlugin.problem( 'type', 'test' ),
        solutions = TestPlugin.solutions( problem );

    solutions.should.be.an.Array;
    solutions.length.should.eql(1);
    solutions[0].should.be.an.Object;
    let sol = solutions[0];
    sol.should.have.property('type', 'type');
    sol.should.have.property('solver', 'solver');
    sol.should.have.property('solution', 'test');
  });

  it( 'solutions (many solvers)', () => {
    let TestPlugin = Plugin( 'TestPlugin' );

    TestPlugin.loader( 'type', (data) => {
      return { problem: data };
    });

    TestPlugin.solver( 'type', 'solver1', (problem) => {
      return 'test1';
    });

    TestPlugin.solver( 'type', 'solver2', (problem) => {
      return 'test2';
    });

    let info = TestPlugin.info();
    info.solvers.length.should.eql(2);
    info = new Set(info.solvers);
    info.has('solver1').should.eql(true);
    info.has('solver2').should.eql(true);

    let problem = TestPlugin.problem( 'type', 'test' ),
        solutions = TestPlugin.solutions( problem );

    solutions.should.be.an.Array;
    solutions.length.should.eql(2);

    let sol = new Set( solutions.map( (s) => s.solver ) );
    sol.has('solver1').should.eql(true);
    sol.has('solver2').should.eql(true);

    sol = new Set( solutions.map( (s) => s.solution ) );
    sol.has('test1').should.eql(true);
    sol.has('test2').should.eql(true);
  });

  /*
  it( 'modules', () => {
    let TestPlugin = Plugin( 'TestPluginModule' );

    let info = TestPlugin.info();
    
    let modules = new Set( info.modules );
    modules.has('Module1').should.eql(true);
    modules.has('Module2').should.eql(true);

    let problems = new Set( info.problems );
    problems.has('problem').should.eql(true);

    let solvers = new Set( info.solvers );
    solvers.has('solver1').should.eql(true);
    solvers.has('solver2').should.eql(true);
  });
  */

}); // Plugin

